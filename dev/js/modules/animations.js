/*
 * @Author: duezerouno 
 * @Date: 2017-08-29 23:38:14 
 * @Last Modified by: duezerouno
 * @Last Modified time: 2017-08-30 20:24:11
 */

var AOS = require('vendor/aos');

AOS.init({
  duration: 300,
  easing: 'ease-in-sine',
  startEvent: 'load'
});