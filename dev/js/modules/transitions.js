/*
 * @Author: duezerouno 
 * @Date: 2017-08-29 23:37:40 
 * @Last Modified by: duezerouno
 * @Last Modified time: 2017-08-30 20:29:41
 */

var Barba = require('vendor/barba');
var AOS = require('vendor/aos');

document.addEventListener("DOMContentLoaded", function() {    
    
    Barba.Pjax.start();
    Barba.Prefetch.init();

    Barba.Pjax.getTransition = function () {
    return FadeTransition;
    };
    

    var FadeTransition = Barba.BaseTransition.extend({
    start: function () {
        Promise
        .all([this.newContainerLoading, this.fadeOut()])
        .then(this.fadeIn.bind(this));
    },

    fadeOut: function () {
        return $(this.oldContainer).animate({
        opacity: 0,
        duration: 1000
        }).promise();
    },

    fadeIn: function () {
        var _this = this;
        var $el = $(this.newContainer);

        $(this.oldContainer).hide();

        $el.css({
        visibility: 'visible',
        opacity: 0
        });

        $el.animate({
        opacity: 1
        }, 1000, function () {
        _this.done();
        });
    }
    });

    Barba.Dispatcher.on('transitionCompleted', function(currentStatus, oldStatus, container) {

        AOS.refresh();
        
    });

});